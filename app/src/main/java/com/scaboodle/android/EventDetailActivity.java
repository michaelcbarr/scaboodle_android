package com.scaboodle.android;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.scaboodle.android.fragments.AttendeesFragment;
import com.scaboodle.android.fragments.CommentsFragment;
import com.scaboodle.android.fragments.DetailsFragment;
import com.scaboodle.android.fragments.DirectionsFragment;
import com.scaboodle.android.model.Event;
import com.scaboodle.android.model.LocationObject;
import com.scaboodle.android.model.UserModel;
import com.scaboodle.android.network.AsyncRestClient;
import com.squareup.picasso.Picasso;

public class EventDetailActivity extends ActionBarActivity {

	private static final String TAG = EventDetailActivity.class.getName();

	// button controls
	private boolean DETAILS = false;
	private boolean ATTENDEES = false;
	private boolean COMMENTS = false;
	private boolean DIRECTIONS = false;
	private boolean ATTENDING = false;

	// event id passed in
	private int eventId;
	private AsyncHttpClient asyncClient;

	// setting up fragments and fragment manager
	AttendeesFragment attendeesFrag;
	CommentsFragment commentsFrag;
	DirectionsFragment directionsFrag;
	DetailsFragment detailsFrag;

	FragmentManager fm;
	FragmentTransaction transaction;

	// model objects
	LocationObject location;
	Event details;
	UserModel userModel;

	// setting up buttons
	private Button attendButton, withdrawButton;
	private RelativeLayout attendeeButton, commentsButton, detailButton, directionsButton;
	TextView attTv1, comTv1, detTv1, dirTv1;
	TextView attTv2, comTv2, detTv2, dirTv2;
	ImageView attIv, comIv, detIv, dirIv;
	ImageView eventBanner;

	public ShareActionProvider mShareActionProvider;

	public JSONObject json;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_view_activity);
	    
	    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	    getSupportActionBar().setHomeButtonEnabled(true);

		asyncClient = AsyncRestClient.getClient(this);
		
		// grab the event id
		Intent intent = getIntent();
		eventId = intent.getIntExtra("eventId", -1);
		setTitle(intent.getStringExtra("eventName"));

		initialiseButtons();

		// setting up fragments
		attendeesFrag = new AttendeesFragment();
		commentsFrag = new CommentsFragment();
		detailsFrag = new DetailsFragment();
		directionsFrag = DirectionsFragment.newInstance(location);

		eventBanner = (ImageView) findViewById(R.id.bannerActivityView);
		eventBanner.setImageResource(R.drawable.default_banner);

		// setting up adding fragments to the transaction
		// map view is excluded for now TODO fix
		transaction = fm.beginTransaction();
		transaction.add(R.id.attendees_fragment_frameActivityView, attendeesFrag);
		transaction.add(R.id.comment_fragment_frameActivityView, commentsFrag);
		transaction.add(R.id.details_fragment_frameActivityView, detailsFrag);
		transaction.add(R.id.directions_fragment_frameActivityView, directionsFrag);
		transaction.commit();

		showDetailFragment();

		startAsyncTasks(eventId);

		// Capture position and set results to the TextViews
		// Capture position and set results to the ImageView
		// Passes flag images URL into ImageLoader.class to download and cache
		// images

	}
	
	// enable back from action bar
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
	
	public void getDefaultShareIntent(String name) {
		String extraText = name + " " + "http://scaboodle.com/#/event/" + eventId;
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_SUBJECT, name);
		intent.putExtra(Intent.EXTRA_TEXT, extraText);
		if (mShareActionProvider != null)
			mShareActionProvider.setShareIntent(intent);
	}

	private void hideFragments() {
		transaction = fm.beginTransaction();
		transaction.hide(attendeesFrag);
		transaction.hide(commentsFrag);
		transaction.hide(detailsFrag);
		transaction.hide(directionsFrag);
		transaction.commit();
	}

	private void startAsyncTasks(int eventId) {

		// kicking off the async tasks to gather the event details
		Log.i(TAG, "Starting async tasks...");
		getCurrentMemberDetails();
		detailsFrag.startAsyncTask(this);

		getEventLocation(eventId);

		String bannerUrl = "http://scaboodle.com/avatars/eventbanner/" + eventId + "/banner";
		Picasso.with(this).load(bannerUrl).into(eventBanner);
	}

	public void getEventLocation(int eventId) {
		String jsonURL = "http://scaboodle.com/events/json/eventdetail/" + eventId;
		asyncClient.get(jsonURL, new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
				super.onFailure(statusCode, headers, responseBody, e);
				Log.e(TAG, "Error getting Location: " + e.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONObject json) {
				super.onSuccess(json);
				try {
					JSONObject locationJson = json.getJSONObject("location");
					createLocationObject(locationJson);
					updateMapPin();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	public void initialiseButtons() {
		detailButton = (RelativeLayout) findViewById(R.id.detailButtonActivityView);
		detTv1 = (TextView) detailButton.getChildAt(1);
		detTv2 = (TextView) detailButton.getChildAt(2);
		detIv = (ImageView) detailButton.getChildAt(0);
		setupDetailButton();
		detailButton.setHapticFeedbackEnabled(true);

		attendeeButton = (RelativeLayout) findViewById(R.id.attendeeButtonActivityView);
		attTv1 = (TextView) attendeeButton.getChildAt(1);
		attTv2 = (TextView) attendeeButton.getChildAt(2);
		attIv = (ImageView) attendeeButton.getChildAt(0);
		setupAttendeeButton("-");
		attendeeButton.setHapticFeedbackEnabled(true);

		commentsButton = (RelativeLayout) findViewById(R.id.commentButtonActivityView);
		comTv1 = (TextView) commentsButton.getChildAt(1);
		comTv2 = (TextView) commentsButton.getChildAt(2);
		comIv = (ImageView) commentsButton.getChildAt(0);
		setupCommentButton("-");
		commentsButton.setHapticFeedbackEnabled(true);

		directionsButton = (RelativeLayout) findViewById(R.id.directionButtonActivityView);
		dirTv1 = (TextView) directionsButton.getChildAt(1);
		dirTv2 = (TextView) directionsButton.getChildAt(2);
		dirIv = (ImageView) directionsButton.getChildAt(0);
		setupDirectionButton();
		directionsButton.setHapticFeedbackEnabled(true);

		attendButton = (Button) findViewById(R.id.attendButtonActivityView);
		withdrawButton = (Button) findViewById(R.id.withdrawButtonActivityView);
		checkAttending(ATTENDING);

		fm = getSupportFragmentManager();

		detailButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				showDetailFragment();
			}
		});

		attendeeButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				showAttendeesFragment();
			}
		});

		commentsButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				showCommentsFragment();
			}
		});

		directionsButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				showDirectionsFragment();
			}
		});

		attendButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				ATTENDING = true;
				checkAttending(true);
			}
		});

		withdrawButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				ATTENDING = false;
				checkAttending(false);
			}
		});
	}

	public void checkAttending(Boolean attending) {
		if (attending) {
			attendButton.setVisibility(View.INVISIBLE);
			withdrawButton.setVisibility(View.VISIBLE);
		} else {
			attendButton.setVisibility(View.VISIBLE);
			withdrawButton.setVisibility(View.INVISIBLE);
		}
	}

	public void setupAttendeeButton(String noOfAttendees) {
		attTv1.setText(noOfAttendees);
		attTv2.setText("ATTENDEES");
		attIv.setImageResource(R.drawable.attendees40);
	}

	public void setupCommentButton(String noOfComments) {
		comTv1.setText(noOfComments);
		comTv2.setText("COMMENTS");
		comIv.setImageResource(R.drawable.comment40);
	}

	public void setupDetailButton() {
		detTv1.setText("INFO");
		detTv2.setText("& OPTIONS");
		detIv.setImageResource(R.drawable.detailinfo40x40);
	}

	public void setupDirectionButton() {
		dirTv1.setText("HOW TO GET THERE");
		dirTv2.setText("");
		dirTv1.setTextSize(15);
		dirIv.setImageResource(R.drawable.howtogetthere40);
	}

	// Transitioning to each of the fragments
	public void showDetailFragment() {
		if (!DETAILS) {
			detailButton.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			falsifyButtons();
			hideFragments();
			transaction = fm.beginTransaction();
			transaction.show(detailsFrag);
			transaction.commit();
			DETAILS = true;
			updateButtonUiOnClick();
		}
	}

	public void showAttendeesFragment() {
		if (!ATTENDEES) {
			attendeeButton.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			falsifyButtons();
			hideFragments();
			transaction = fm.beginTransaction();
			transaction.show(attendeesFrag);
			transaction.commit();

			ATTENDEES = true;
			updateButtonUiOnClick();
		}
	}

	public void showCommentsFragment() {
		if (!COMMENTS) {
			commentsButton.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			falsifyButtons();
			hideFragments();
			transaction = fm.beginTransaction();
			transaction.show(commentsFrag);
			transaction.commit();

			COMMENTS = true;
			updateButtonUiOnClick();
		}
	}

	public void showDirectionsFragment() {
		if (!DIRECTIONS) {
			directionsButton.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
			falsifyButtons();
			hideFragments();
			transaction = fm.beginTransaction();
			transaction.show(directionsFrag);
			transaction.commit();

			DIRECTIONS = true;
			updateButtonUiOnClick();
		}
	}

	private void updateButtonUiOnClick() {

		if (DETAILS == true) {
			detailButton.setBackgroundColor(getResources().getColor(R.color.scaboodle_orange));
		} else {
			detailButton.setBackgroundColor(getResources().getColor(R.color.event_button_blue));
		}
		if (ATTENDEES == true) {
			attendeeButton.setBackgroundColor(getResources().getColor(R.color.scaboodle_orange));
		} else {
			attendeeButton.setBackgroundColor(getResources().getColor(R.color.event_button_blue));
		}
		if (COMMENTS == true) {
			commentsButton.setBackgroundColor(getResources().getColor(R.color.scaboodle_orange));
		} else {
			commentsButton.setBackgroundColor(getResources().getColor(R.color.event_button_blue));
		}
		if (DIRECTIONS == true) {
			directionsButton.setBackgroundColor(getResources().getColor(R.color.scaboodle_orange));
		} else {
			directionsButton.setBackgroundColor(getResources().getColor(R.color.event_button_blue));
		}
	}

	private void falsifyButtons() {
		DETAILS = false;
		ATTENDEES = false;
		COMMENTS = false;
		DIRECTIONS = false;
	}

	public void setJSON(JSONObject o) {
		this.json = o;
	}

	public JSONObject getJSON() {
		return this.json;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	// creating fragment objects
	public void createLocationObject(JSONObject json) throws JSONException {
		if (json != null)
			this.location = new LocationObject(json);
	}

	public void createDetailsObject(JSONObject json) throws JSONException {
		this.details = new Event(json);
	}

	public void updateMapPin() {
		directionsFrag.updatePin(location);
	}

	public void setAttending(Boolean bool) {
		this.ATTENDING = bool;
	}

	public void getCurrentMemberDetails() {
		String jsonURL = "http://scaboodle.com/members/json/user";
		Log.i(TAG, "Getting current user data");
		JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable error) {
				super.onFailure(statusCode, headers, response, error);
				Log.e(TAG, "Error: " + error.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONObject json) {
				super.onSuccess(json);
				processCurrentMemberData(json);
			}
		};
		handler.setUseSynchronousMode(true);
		asyncClient.get(jsonURL, handler);
	}

	private void processCurrentMemberData(JSONObject json) {
		Log.i(TAG, "Got current user data");
		try {
			userModel = new UserModel(json.getJSONObject("member"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		attendeesFrag.startAsyncTask(userModel);
	}
}

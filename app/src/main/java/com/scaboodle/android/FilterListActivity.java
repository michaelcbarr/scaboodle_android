package com.scaboodle.android;

/**
 * @author Steven McStravog
 * 
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

public class FilterListActivity extends ActionBarActivity {
	private static final String TAG = FilterListActivity.class.getName();
	Button filterListButton;

	private AsyncHttpClient asyncClient;

	Typeface font;
	int buttonCount = 1;

	List<String> buttonIdArray = new ArrayList<String>();
	List<String> buttonEventCount = new ArrayList<String>();

	private ActionBarDrawerToggle mDrawerToggle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		UnsupportedClassVersionError UnsupportedClassVersionError = new java.lang.UnsupportedClassVersionError(
				"This class doesn't work and should not be used");
		throw UnsupportedClassVersionError;
		/*
		 * super.onCreate(savedInstanceState);
		 * setContentView(R.layout.activity_filter_list);
		 * 
		 * asyncClient = AsyncRestClient.getClient(this);
		 * 
		 * font = Typeface.createFromAsset(getApplicationContext().getAssets(),
		 * "HelveticaNeueCondensedBold.ttf"); ImageView img = (ImageView)
		 * findViewById(R.id.imageViewFilterListLogo); img.setId(999);
		 */
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_filter_list, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void listEventCountAsync() {
		for (int i = 0; i < buttonIdArray.size(); i++) {
			final int index = i + 1;
			String jsonURL = ("http://scaboodle.com/customfilter/json/" + buttonIdArray.get(i) + "?page=1");
			asyncClient.get(jsonURL, new JsonHttpResponseHandler() {
				@Override
				public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
					super.onFailure(statusCode, headers, responseBody, e);
					String tag = "Filter Count Error";
					Log.e(tag, "Error: " + e.getLocalizedMessage());
					Log.e(tag, "Response: " + responseBody);
				}

				@Override
				public void onSuccess(JSONObject response) {
					super.onSuccess(response);
					generateListCount(response, index);
				}
			});
		}
	}

	public void generateListButtons(JSONArray jArray) {
		if (jArray != null) {
			try {
				for (int bId = 0; bId < jArray.length(); bId++) {
					JSONObject streamJson = jArray.getJSONObject(bId);
					createStreamButton(streamJson, bId);
				}
				createStreamButton(new JSONObject("{name:\"attending\", id:\"attending\"}"), jArray.length());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	private void createStreamButton(JSONObject streamJson, int bId) throws JSONException {
		String listName = streamJson.getString("name");

		if (listName.length() > 16) {
			listName = listName.substring(0, 16) + "...";
		}

		String listId = streamJson.getString("id");

		buttonIdArray.add(listId);

		filterListButton = new Button(this);
		filterListButton.setText(listName.toUpperCase() + ": ");
		filterListButton.setGravity(Gravity.CENTER | Gravity.LEFT);
		filterListButton.setId(bId + 1);
		filterListButton.setTypeface(font);
		filterListButton.setTextSize(24);
		filterListButton.setBackgroundResource(R.drawable.filter_list_button);
		filterListButton.setTextColor(Color.parseColor("#FFFFFF"));
		Drawable arrowImage = getResources().getDrawable(R.drawable.arrowhead);
		filterListButton.setCompoundDrawablesWithIntrinsicBounds(null, null, arrowImage, null);

		filterListButton.setOnClickListener(getEventList(filterListButton));

		RelativeLayout layout = (RelativeLayout) findViewById(R.id.RelativeLayoutLayoutFilterListScrollView);
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		if (bId == 0) {
			rlp.addRule(RelativeLayout.BELOW, 999);
			rlp.topMargin = 25;
		} else {
			rlp.addRule(RelativeLayout.BELOW, bId);
			rlp.topMargin = 10;
		}
		rlp.height = 65;
		rlp.leftMargin = 25;
		rlp.rightMargin = 25;
		layout.addView(filterListButton, rlp);

	}

	public void generateListCount(JSONObject json, int buttonIndex) {
		if (json != null) {
			String totalElements = null;
			try {
				JSONObject pObj = json.getJSONObject("page");
				totalElements = pObj.getString("totalElements");
				Button lButton = (Button) findViewById(buttonIndex);
				lButton.append(totalElements);
				buttonEventCount.add(totalElements);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	View.OnClickListener getEventList(final Button button) {
		return new View.OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG, "Clicked button with ID: " + button.getId());
				int id = button.getId() - 1;
				String streamId = buttonIdArray.get(id);
				String listName = button.getText().toString();
				String eventCount = buttonEventCount.get(id);
				Intent EventList = new Intent(FilterListActivity.this, EventListActivity.class);
				Bundle extras = new Bundle();
				extras.putString("streamId", streamId);
				extras.putString("listName", listName);
				extras.putString("eventCount", eventCount);

				EventList.putExtras(extras);
				FilterListActivity.this.startActivity(EventList);
			}
		};
	}

}

package com.scaboodle.android.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scaboodle.android.R;
import com.scaboodle.android.model.EventComment;
import com.squareup.picasso.Picasso;

public class EventCommentAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<EventComment> data;
	Typeface font;

	public EventCommentAdapter(Context context, ArrayList<EventComment> arraylist) {
		this.context = context;
		data = arraylist;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.event_comment_list_item, parent, false);
		// Get the position from the results

		EventComment comment = data.get(position);

		// Set text in TextViews
		TextView name = (TextView) itemView.findViewById(R.id.commentName);
		TextView commentText = (TextView) itemView.findViewById(R.id.commentText);

		name.setText(comment.getName());
		commentText.setText(comment.getComment());

		// Set avatar ImageView
		ImageView avatar = (ImageView) itemView.findViewById(R.id.commentImage);
		Picasso.with(context)
			.load(comment.getProfilePicUrl())
			.resize(100, 100)
			.placeholder(R.drawable.ic_launcher)
			.into(avatar);
		
		// Capture button clicks on ListView items
		itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
			}
		});

		return itemView;
	}
}
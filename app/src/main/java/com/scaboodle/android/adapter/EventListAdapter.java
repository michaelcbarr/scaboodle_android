package com.scaboodle.android.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.scaboodle.android.EventDetailActivity;
import com.scaboodle.android.R;
import com.scaboodle.android.model.Event;
import com.squareup.picasso.Picasso;

public class EventListAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<Event> events;
	Typeface font;

	public EventListAdapter(Context context, ArrayList<Event> events) {
		this.context = context;
		this.events = events;
		font = Typeface.createFromAsset(context.getAssets(), "HelveticaNeueCondensedBold.ttf");
	}

	@Override
	public int getCount() {
		return events.size();
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView title;
		TextView desc;
		TextView date;
		ImageView imgEvent;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.listview_item, parent, false);

		// Get the position from the results
		Event event = events.get(position);

		// Locate the TextViews in listview_item.xml
		title = (TextView) itemView.findViewById(R.id.title);
		desc = (TextView) itemView.findViewById(R.id.desc);
		date = (TextView) itemView.findViewById(R.id.date);
		title.setTypeface(font);

		// Locate the ImageView in listview_item.xml
		imgEvent = (ImageView) itemView.findViewById(R.id.imgEvent);

		title.setText(event.getName());
		desc.setText(event.getDescription());
		date.setText(event.getStartDate());

		// Capture position and set results to the ImageView
		String avatarUrl = "http://scaboodle.com/avatars/event/" + event.getId() + "/large";
		Picasso.with(context).load(avatarUrl).into(imgEvent);

		// Capture button clicks on ListView items
		itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Event event = events.get(position);
				Intent intentEventView = new Intent(context, EventDetailActivity.class);
				intentEventView.putExtra("eventId", event.getId());
				intentEventView.putExtra("eventName", event.getName());
				context.startActivity(intentEventView);
			}
		});

		return itemView;
	}

	@Override
	public Event getItem(int position) {
		return events.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
}
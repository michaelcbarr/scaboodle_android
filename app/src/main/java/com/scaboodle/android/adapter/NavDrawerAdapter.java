package com.scaboodle.android.adapter;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.scaboodle.android.R;
import com.scaboodle.android.network.AsyncRestClient;

public class NavDrawerAdapter extends BaseAdapter {

	private static final String TAG = NavDrawerAdapter.class.getName();

	private Context context;
	private int layout = R.layout.drawer_list_item;
	private ArrayList<String> streamNames = new ArrayList<String>();
	private ArrayList<String> streamURLs = new ArrayList<String>();
	private ArrayAdapter<String> adapter;

	private AsyncHttpClient asyncClient;

	public NavDrawerAdapter(Context context) {
		this.context = context;

		asyncClient = AsyncRestClient.getClient(context);

		adapter = new ArrayAdapter<String>(context, layout, streamNames);
		streamNames.add("Logout");
		// add empty URL to keep the 2 arrays in sync
		streamURLs.add("");

		streamNames.add("Buzzing");
		streamURLs.add("http://scaboodle.com/events/json/buzzlist");
		streamNames.add("Attending");
		streamURLs.add("http://scaboodle.com/customfilter/json/attending");
		streamNames.add("Maybe");
		streamURLs.add("http://scaboodle.com/customfilter/json/maybe");
		streamNames.add("Managing");
		streamURLs.add("http://scaboodle.com/customfilter/json/managing");
		streamNames.add("Friends Activities");
		streamURLs.add("http://scaboodle.com/customfilter/json/friendsactivities");
		notifyDataSetChanged();
		
		String jsonURL = ("http://scaboodle.com/customfilter/json/user");
		JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
				super.onFailure(statusCode, headers, responseBody, e);
				Log.e(TAG, "Error: " + e.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONArray response) {
				super.onSuccess(response);
				addStreams(response);
			}
		};
		handler.setUseSynchronousMode(true);
		asyncClient.get(jsonURL, handler);
	}

	public void add(String s) {
		streamNames.add(s);
	}

	@Override
	public int getCount() {
		return streamNames.size();
	}

	@Override
	public String getItem(int position) {
		return streamNames.get(position);
	}

	public String getStreamUrl(int position) {
		return streamURLs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return adapter.getView(position, convertView, parent);
	}

	public void addStreams(JSONArray jArray) {
		if (jArray != null) {
			try {
				for (int i = 0; i < jArray.length(); i++) {
					JSONObject streamJson = jArray.getJSONObject(i);
					streamNames.add(streamJson.getString("name"));
					streamURLs.add("http://scaboodle.com/customfilter/json/" + streamJson.getString("id"));
				}
				notifyDataSetChanged();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
}

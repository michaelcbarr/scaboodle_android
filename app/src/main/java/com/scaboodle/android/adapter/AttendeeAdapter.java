package com.scaboodle.android.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.scaboodle.android.R;
import com.scaboodle.android.model.Attendee;
import com.squareup.picasso.Picasso;

public class AttendeeAdapter extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<Attendee> data;
	Typeface font;

	public AttendeeAdapter(Context context, ArrayList<Attendee> arraylist) {
		this.context = context;
		data = arraylist;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		ImageView imgProfilePic;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.event_attendee_grid_item, parent, false);
		// Get the position from the results

		Attendee resultp = new Attendee();
		resultp = data.get(position);

		imgProfilePic = (ImageView) itemView.findViewById(R.id.attendeeImage);

		// Capture position and set results to the TextViews
		// Capture position and set results to the ImageView
		// Passes flag images URL into ImageLoader.class to download and cache
		// images

		Picasso.with(this.context)
			.load(resultp.getMemberProfileUrl())
			.placeholder(R.drawable.ic_launcher)
			.resize(200, 200)
			.into(imgProfilePic);
		
		// Capture button clicks on ListView items
		itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get the position from the results
				// HashMap<String, String> resultp = new HashMap<String,
				// String>();
				// resultp = data.get(position);
				// Send single item click data to SingleItemView Class
				// Intent intent = new Intent(context, SingleItemView.class);
				// Pass all data rank
				// intent.putExtra("rank", resultp.get(MainActivity.RANK));
				// Pass all data country
				// intent.putExtra("country",
				// resultp.get(MainActivity.COUNTRY));
				// Pass all data population
				// intent.putExtra("population",
				// resultp.get(MainActivity.POPULATION));
				// Pass all data flag
				// intent.putExtra("flag", resultp.get(MainActivity.FLAG));
				// Start SingleItemView Class
				// context.startActivity(intent);

			}
		});

		return itemView;
	}
}
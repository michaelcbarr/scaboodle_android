package com.scaboodle.android;

/**
 * @author Steven McStravog
 * 
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.scaboodle.android.adapter.EventListAdapter;
import com.scaboodle.android.adapter.NavDrawerAdapter;
import com.scaboodle.android.model.Event;
import com.scaboodle.android.network.AsyncRestClient;

public class EventListActivity extends ActionBarActivity {
	private static final String TAG = EventListActivity.class.getName();

	private AsyncHttpClient asyncClient;
	private ProgressDialog progressDialog;
	
	RelativeLayout customLinearLayout = null;
	String streamUrl;
	String listName;
	
	private NavDrawerAdapter mNavAdapter;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView mDrawerList;

	private int page = 1;

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	ArrayList<Event> events = new ArrayList<Event>();

	ListView listView;
	EventListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "Event List Activity created");
		setContentView(R.layout.activity_event_list);
		
		asyncClient = AsyncRestClient.getClient(this);
		progressDialog = new ProgressDialog(this);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
				mDrawerLayout, /* DrawerLayout object */
				R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description */
				R.string.drawer_close /* "close drawer" description */
		);

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		// Set the adapter for the list view
		mNavAdapter = new NavDrawerAdapter(this);
		mDrawerList.setAdapter(mNavAdapter);
		
		// Set the list's click listener
		listView = (ListView) findViewById(R.id.listview);
		listView.setOnItemClickListener(new EventClickListener());

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		streamUrl = extras.getString("streamUrl");
		listName = extras.getString("listName");
		getActionBar().setTitle(listName);

		eventListAsync();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_event_list, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case android.R.id.home:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void eventListAsync() {
		String jsonURL = streamUrl + "?page=" + String.valueOf(page);
		progressDialog.setMessage("Loading events");
		progressDialog.setCancelable(false);
		progressDialog.show();

		Log.i(TAG, "Getting page " + String.valueOf(page));
		asyncClient.get(jsonURL, new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
				super.onFailure(statusCode, headers, responseBody, e);
				progressDialog.dismiss();
				Toast errorToast = new Toast(EventListActivity.this);
				errorToast.setText("Couldn't load events :(");
				errorToast.show();
				Log.e(TAG, "Error: " + e.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONObject response) {
				super.onSuccess(response);
				Log.i(TAG, "Got event list for stream: " + streamUrl);
				generateEventListData(response);
			}
		});
	}

	public void generateEventListData(JSONObject json) {
		try {
			JSONArray arr = json.getJSONArray("content");
			for (int i = 0; i < arr.length(); i++) {
				Event event = new Event(arr.getJSONObject(i));
				events.add(event);
			}
			page++;
			int totalPages = Integer.valueOf(json.getJSONObject("page").getInt("totalPages"));
			if (page <= totalPages && page < 4) {
				eventListAsync();
			} else {
				progressDialog.dismiss();
				adapter = new EventListAdapter(EventListActivity.this, events);
				listView.setAdapter(adapter);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private class EventClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			listView.setItemChecked(position, true);
		}
	}
	
	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	}

	/** Swaps fragments in the main content view */
	private void selectItem(int position) {
		// logout
		if (position == 0)
		//if (position == (mNavAdapter.getCount() - 1))
		{
			progressDialog = new ProgressDialog(this);
			progressDialog.setMessage("Logging out...");
			progressDialog.setCancelable(false);

			String jsonURL = ("http://scaboodle.com/logout/");
			AsyncHttpResponseHandler logoutHandler = new AsyncHttpResponseHandler() {
				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable e) {
					super.onFailure(statusCode, headers, responseBody, e);
					Log.e(TAG, "Error: " + e.getLocalizedMessage());
				}

				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] content) {
					super.onSuccess(statusCode, headers, content);
					progressDialog.dismiss();
					AsyncRestClient.getCookieStore().clear();
					Intent Login = new Intent(EventListActivity.this, LoginActivity.class);
					EventListActivity.this.startActivity(Login);
				}
			};
			asyncClient.get(jsonURL, logoutHandler);
		}
		else {
			listName = mNavAdapter.getItem(position);
			getActionBar().setTitle(listName);
			streamUrl = mNavAdapter.getStreamUrl(position);
			page = 1;
			events.clear();
			eventListAsync();
		}
		
		// Highlight the selected item, update the title, and close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle((String)mNavAdapter.getItem(position));
		mDrawerLayout.closeDrawer(mDrawerList);
	}
}

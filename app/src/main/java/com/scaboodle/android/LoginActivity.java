package com.scaboodle.android;

/**
 * @author Steven McStravog
 * 
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.scaboodle.android.network.LoginManager;

public class LoginActivity extends FragmentActivity {
	private EditText etEmailAddress;
	private EditText etPassword;
	private Button bLogin;
	private ProgressDialog progressDialog;
	private static final String TAG = LoginActivity.class.getName();
	private LoginManager loginManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO add shared preferences and implement a single sign in
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String username = settings.getString("username", "");

		etEmailAddress = (EditText) findViewById(R.id.editTextLoginEmail);
		etEmailAddress.setText(username);

		etPassword = (EditText) findViewById(R.id.editTextLoginPassword);
		if (username != "") {
			etPassword.requestFocus();
		}

		bLogin = (Button) findViewById(R.id.buttonLoginSignIn);
		bLogin.setOnClickListener(loginOnClickListener);
		
		progressDialog = new ProgressDialog(LoginActivity.this);
		progressDialog.setMessage("Logging in...");
		progressDialog.setCancelable(false);

		Log.i(TAG, "Creating Login manager");
		loginManager = new LoginManager(LoginActivity.this, progressDialog);
	}

	protected OnClickListener loginOnClickListener = new OnClickListener() {
		public void onClick(View v) {
			loginManager.performLogin(etEmailAddress.getText().toString(), etPassword.getText().toString());
		}
	};

	public void showLoginError(String result) {
		AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
		builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		builder.setMessage(R.string.login_invalid_message);
		AlertDialog alert = builder.create();
		alert.setCancelable(false);
		alert.show();
	}

	// start FilterListActivity after login
	public void login() {
		Intent buzzing = new Intent(LoginActivity.this, EventListActivity.class);
		buzzing.putExtra("streamUrl", "http://scaboodle.com/events/json/buzzlist");
		buzzing.putExtra("listName", "Buzzing");
		LoginActivity.this.startActivity(buzzing);
	}

}

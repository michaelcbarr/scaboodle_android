package com.scaboodle.android;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;

public class Map extends ActionBarActivity {
	static final LatLng BELFAST = new LatLng(54.547323, -6.009521);
	private GoogleMap map;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		map = ((MapFragment) getFragmentManager().findFragmentById(
				R.id.mapLayoutFrag)).getMap();
		//Marker belfast = map.addMarker(new MarkerOptions().position(BELFAST).title("Belfast"));

		// setting the options for the map (zoom controls, map type etc.)
		UiSettings opt = map.getUiSettings();
		opt.setZoomControlsEnabled(false);
		opt.setMyLocationButtonEnabled(true);

		// Move the camera instantly to belfast with a zoom of 15.
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(BELFAST, 15));

		// Zoom in, animating the camera.
		map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
	}
}

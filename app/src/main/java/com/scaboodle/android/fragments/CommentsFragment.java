package com.scaboodle.android.fragments;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.scaboodle.android.EventDetailActivity;
import com.scaboodle.android.adapter.EventCommentAdapter;
import com.scaboodle.android.model.EventComment;
import com.scaboodle.android.network.AsyncRestClient;

public class CommentsFragment extends ListFragment {

	private static final String TAG = CommentsFragment.class.getName();
	private AsyncHttpClient asyncClient;

	ArrayList<EventComment> modelArray;
	EventCommentAdapter adapter;
	EventDetailActivity activity;
	String noOfComments = "-";

	int eventId;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.activity = (EventDetailActivity) getActivity();
		this.eventId = activity.getEventId();
		
		asyncClient = AsyncRestClient.getClient(getActivity());
		
		modelArray = new ArrayList<EventComment>();
		adapter = new EventCommentAdapter(getActivity(), modelArray);
		setListAdapter(adapter);
		fetchCommentData();
	}

	private void fetchCommentData() {
		String jsonURL = "http://scaboodle.com/events/json/comments/" + eventId + "?page=1&reverseOrder=true";
		Log.i(TAG, "Getting comment data");
		JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable error) {
				super.onFailure(statusCode, headers, response, error);
				Log.e(TAG, "Error: " + error.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONObject json) {
				super.onSuccess(json);
				processCommentData(json);
			}
		};
		handler.setUseSynchronousMode(true);
		asyncClient.get(jsonURL, handler);
	}

	private void processCommentData(JSONObject json) {
		Log.i(TAG, "Got comment data");
		JSONArray commentsJson;
		try {
			commentsJson = json.getJSONArray("content");
			for (int i = 0; i < commentsJson.length(); i++) {
				JSONObject commentText = commentsJson.getJSONObject(i).getJSONObject("eventComment");
				String memberName = commentsJson.getJSONObject(i).getString("memberName");
				modelArray.add(new EventComment(commentText, memberName));
			}

			noOfComments = json.getJSONObject("page").getString("totalElements");
			activity.setupCommentButton(noOfComments);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

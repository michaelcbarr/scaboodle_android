package com.scaboodle.android.fragments;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.scaboodle.android.EventDetailActivity;
import com.scaboodle.android.R;
import com.scaboodle.android.adapter.AttendeeAdapter;
import com.scaboodle.android.model.Attendee;
import com.scaboodle.android.model.UserModel;
import com.scaboodle.android.network.AsyncRestClient;

public class AttendeesFragment extends Fragment {

	private static final String TAG = AttendeesFragment.class.getName();
	AttendeeAdapter attendeeAdapter;
	ArrayList<Attendee> modelArray;
	Attendee attendeeModel;
	GridView gridview;
	UserModel userModel;
	Boolean isAttending = false;

	EventDetailActivity activity;
	int eventId;
	String noOfAttendees = "-";

	private AsyncHttpClient asyncClient;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.activity_attendees_fragment, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		gridview = (GridView) getView().findViewById(R.id.attendeeGridView);
		modelArray = new ArrayList<Attendee>();
		attendeeModel = new Attendee();

		attendeeAdapter = new AttendeeAdapter(getActivity(), modelArray);

		gridview.setAdapter(attendeeAdapter);
	}

	public void startAsyncTask(UserModel userModel) {
		this.userModel = userModel;
		this.activity = (EventDetailActivity) getActivity();
		this.eventId = activity.getEventId();

		// networking
		asyncClient = AsyncRestClient.getClient(activity);

		String jsonURL = "http://scaboodle.com/attendees/json/event/" + eventId + "/1?reverse=true";
		Log.i(TAG, "Getting attendee data for event: " + eventId);
		JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
				super.onFailure(statusCode, headers, responseBody, e);
				Log.e(TAG, "Error: " + e.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONObject json) {
				super.onSuccess(json);
				Log.i(TAG, "Got attendee data");
				processAttendeeData(json);
			}
		};
		handler.setUseSynchronousMode(true);
		asyncClient.get(jsonURL, handler);
	}

	private void processAttendeeData(JSONObject json) {
		JSONArray attendeesJson;
		try {
			attendeesJson = json.getJSONArray("content");

			// add attendees
			for (int i = 0; i < attendeesJson.length(); i++) {
				modelArray.add(new Attendee(attendeesJson.getJSONObject(i)));
			}

			// check if current user is attending
			for (int i = 0; i < modelArray.size(); i++) {
				if (modelArray.get(i).getMemberId() == userModel.getId()) {
					isAttending = true;
				}
			}

			// get attendee count
			noOfAttendees = json.getJSONObject("page").getString("totalElements");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.activity.setAttending(isAttending);
		this.activity.checkAttending(isAttending);
		this.activity.setupAttendeeButton(noOfAttendees);
	}
}

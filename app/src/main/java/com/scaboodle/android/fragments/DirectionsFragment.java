package com.scaboodle.android.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.scaboodle.android.R;
import com.scaboodle.android.model.LocationObject;

public class DirectionsFragment extends SupportMapFragment {
	private static final String TAG = DirectionsFragment.class.getName();

	private static LatLng locationPin;

	public DirectionsFragment() {
		super();
	}

	public void setupMap() {
		UiSettings settings = getMap().getUiSettings();
		settings.setAllGesturesEnabled(true);
		settings.setMyLocationButtonEnabled(false);
	}

	public static DirectionsFragment newInstance(LocationObject location) {
		DirectionsFragment frag = new DirectionsFragment();

		if (location != null) {
			locationPin = new LatLng(location.getLatitude(), location.getLongitude());
		}
		return frag;
	}

	public void updatePin(LocationObject location) {
		if (location != null) {
			locationPin = new LatLng(location.getLatitude(), location.getLongitude());
			setMapLocation();
		}
	}

	private void setMapLocation() {
		if (getMap() == null)
			// map not created
			return;

		Log.i(TAG, "Setting map location");
		getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(locationPin, 16));
		getMap().addMarker(new MarkerOptions().position(locationPin).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_blank)));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = super.onCreateView(inflater, container, savedInstanceState);
		setupMap();
		return v;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
	}

}

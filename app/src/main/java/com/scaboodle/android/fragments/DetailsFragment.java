package com.scaboodle.android.fragments;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.scaboodle.android.EventDetailActivity;
import com.scaboodle.android.R;
import com.scaboodle.android.model.Event;
import com.scaboodle.android.network.AsyncRestClient;

public class DetailsFragment extends Fragment {
	private static final String TAG = DetailsFragment.class.getName();

	Event detailsModel;
	String name;
	String desc;
	Typeface font;

	private AsyncHttpClient asyncClient;

	private EventDetailActivity viewEvent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_details_fragment, container, false);
		return view;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		asyncClient.cancelRequests(null, true);
	}

	public void startAsyncTask(EventDetailActivity activity) {
		this.viewEvent = activity;
		int eventId = this.viewEvent.getEventId();

		// networking
		asyncClient = AsyncRestClient.getClient(getActivity());

		String jsonURL = "http://scaboodle.com/api/event/detail/" + eventId;

		Log.i(TAG, "Getting details for event: " + eventId);

		JsonHttpResponseHandler handler = new JsonHttpResponseHandler() {
			@Override
			public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable e) {
				super.onFailure(statusCode, headers, responseBody, e);
				Log.e(TAG, "Error: " + e.getLocalizedMessage());
			}

			@Override
			public void onSuccess(JSONObject json) {
				super.onSuccess(json);
				Log.i(TAG, "Got event details");
				processEventData(json);
			}
		};
		handler.setUseSynchronousMode(true);
		asyncClient.get(jsonURL, handler);
	}

	private void processEventData(JSONObject json) {
		try {
			Event model = new Event(json.getJSONObject("event"));
			updateDetails(model);
			name = model.getName();
			viewEvent.getDefaultShareIntent(name);
			
			// set text for count labels
			int numComments = json.getInt("numComments");
			int numAttendees = json.getInt("numAttendees");
			viewEvent.setupCommentButton(String.valueOf(numComments));
			viewEvent.setupAttendeeButton(String.valueOf(numAttendees));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void updateDetails(Event model) throws JSONException {
		final Activity activity = getActivity();

		if (activity == null || model == null) {
			return;
		}

		TextView eventName = (TextView) activity.findViewById(R.id.EventName);
		TextView eventDate = (TextView) activity.findViewById(R.id.EventDate);
		TextView eventDescription = (TextView) activity.findViewById(R.id.EventDescription);

		eventName.setText(model.getName());
		eventDate.setText(model.getStartDate());
		eventDescription.setText(model.getDescription());
	}
}

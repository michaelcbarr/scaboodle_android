package com.scaboodle.android.network;

/**
 * @author Steven McStravog
 * 
 */
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.scaboodle.android.LoginActivity;

public class LoginManager {

	private static final String TAG = LoginManager.class.getName();

	private AsyncHttpClient asyncClient;
	private ProgressDialog progressDialog;
	private LoginActivity activity;
	private PersistentCookieStore myCookieStore;
	private Boolean existingSession = false;

	private String username;
	private String sessionId;
	private JSONObject json;

	public LoginManager(LoginActivity activity, ProgressDialog progressDialog) {
		this.activity = activity;
		this.progressDialog = progressDialog;

		asyncClient = AsyncRestClient.getClient(activity);
		myCookieStore = AsyncRestClient.getCookieStore();

		checkForExistingSession();
	}

	private void checkForExistingSession() {
		// check for pre-existing session
		List<Cookie> cookies = myCookieStore.getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase("JSESSIONID")) {
				Log.i(TAG, "Existing session: " + cookie.getValue());
				existingSession = true;
				onLogin();
				break;
			}
		}
	}

	public void performLogin(String username, String password) {
		progressDialog.show();
		this.username = username;

		String jsonString = "{ " + "\"username\": \"" + this.username + "\", " + "\"password\": \"" + password + "\" " + "}";
		try {
			json = new JSONObject(jsonString);
		} catch (JSONException e1) {
			e1.printStackTrace();
			activity.showLoginError("Uh oh! You entered some invalid details");
			return;
		}

		asyncClient.addHeader("Accept", "application/json");
		if (!existingSession) {
			myCookieStore.clear();
			asyncClient.setCookieStore(myCookieStore);
			asyncClient.get("http://scaboodle.com/members/json/user", new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					super.onSuccess(statusCode, headers, responseBody);
					retrieveSessionId(headers);
					sendLoginPost(json);
				}
			});
		} else {
			sendLoginPost(json);
		}
	}

	private void sendLoginPost(JSONObject json) {
		try {
			String url = "http://scaboodle.com/api/auth";
			String contentType = "application/json; charset=UTF-8";

			asyncClient.post(activity, url, null, createStringEntity(json), contentType, new AsyncHttpResponseHandler() {
				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
					super.onFailure(statusCode, headers, responseBody, error);
					progressDialog.dismiss();
					Log.e(TAG, "Login error:" + error.getLocalizedMessage());
					activity.showLoginError("Login not successful please check your username and password");
				}

				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
					super.onSuccess(statusCode, headers, responseBody);
					Log.i(TAG, "Login complete - Response code: " + String.valueOf(statusCode));
					onLogin();
				}
			});

		} catch (Exception e) {
			Log.e(TAG, "Error: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	// TODO add more extensive login validation
	private void onLogin() {
		progressDialog.dismiss();

		// successful login, store details
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("username", this.username);
		editor.commit();
		activity.login();
	}

	// from
	// http://stackoverflow.com/questions/14119410/json-not-working-with-httppost-probably-around-setentity
	private static HttpEntity createStringEntity(JSONObject params) {
		StringEntity se = null;
		try {
			se = new StringEntity(params.toString(), "UTF-8");
			se.setContentType("application/json; charset=UTF-8");
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "Failed to create StringEntity", e);
			e.printStackTrace();
		}
		return se;
	}

	private void retrieveSessionId(Header[] headers) {
		String cookie = null;
		for (Header header : headers) {
			if (header.getName().equalsIgnoreCase("Set-Cookie")) {
				cookie = header.getValue();
				break;
			}
		}
		if (cookie == null) {
			// no session id received/needed
			return;
		}

		// got a cookie
		Pattern p = Pattern.compile("JSESSIONID=([A-Za-z0-9]*);");
		Matcher m = p.matcher(cookie);
		while (m.find()) { // Find each match in turn; String can't
			// do this.
			this.sessionId = m.group(1); // Access a submatch group;
			// String can't do this.
		}

		Log.i(TAG, "Got new session ID: " + this.sessionId);
		BasicClientCookie newCookie = new BasicClientCookie("JSESSIONID", this.sessionId);
		newCookie.setDomain("scaboodle.com");
		newCookie.setPath("/");
		myCookieStore.clear();
		myCookieStore.addCookie(newCookie);
		asyncClient.setCookieStore(myCookieStore);
		existingSession = true;
	}
}

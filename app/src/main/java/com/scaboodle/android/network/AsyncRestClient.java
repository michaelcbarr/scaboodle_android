package com.scaboodle.android.network;

import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;

public class AsyncRestClient {

	private static AsyncHttpClient mClient = null;
	private static PersistentCookieStore mCookieStore;

	public static PersistentCookieStore getCookieStore() {
		return mCookieStore;
	}

	public static void setCookieStore(PersistentCookieStore cookieStore) {
		AsyncRestClient.mCookieStore = cookieStore;
	}

	// private constructor that prevents instantiation
	private AsyncRestClient() {

	}

	// activity needs to be passed as cookie store must refer to Application context
	public static synchronized AsyncHttpClient getClient(Context context) {
		if (mClient == null) {
			mClient = new AsyncHttpClient();

			HttpParams params = mClient.getHttpClient().getParams();
			HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);

			mClient.setTimeout(15000);
			mClient.setMaxRetriesAndTimeout(2, 1000);
			;
			mClient.setMaxConnections(12);
			// mClient.addHeader("Accept", "application/json");

			mCookieStore = new PersistentCookieStore(context.getApplicationContext());
			mClient.setCookieStore(mCookieStore);
		}
		return mClient;
	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
}
package com.scaboodle.android.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class Event {

	// http://sandbox.scaboodle.com/events/json/comments/67?page=1&reverseOrder=true
	//
	// http://sandbox.scaboodle.com/attendees/json/event/67/1?reverse=true
	//
	// http://sandbox.scaboodle.com/events/json/eventdetail/67

	private int id;
	private String name;
	private boolean privacy;
	private String[] tags;
	private int cost;
	private int availableSpaces;
	private String registrationStartDate;
	private String registrationEndDate;
	private String startDate;
	private String endDate;
	private String description;
	private int creator;
	private String category;
	private boolean paid;
	private boolean followingCreated;
	private boolean followingAttending;
	private boolean attending;
	private String locationDescription;
	private int[] attendees;
	private int minAge;
	private boolean ageRestriction;
	private String timezoneLongName;
	private String timezoneShortName;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("E d MMM yyyy - h:mm a");

	public Event(JSONObject json) throws JSONException {
		setId(json.getInt("id"));
		setName(json.getString("name"));
		setStartDate(json.getString("startDate"));
		// setEndDate(json.getString("endDate"));
		setDescription(json.optString("description"));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPrivacy() {
		return privacy;
	}

	public void setPrivacy(boolean privacy) {
		this.privacy = privacy;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getAvailableSpaces() {
		return availableSpaces;
	}

	public void setAvailableSpaces(int availableSpaces) {
		this.availableSpaces = availableSpaces;
	}

	public String getRegistrationStartDate() {
		return registrationStartDate;
	}

	public void setRegistrationStartDate(String registrationStartDate) {
		this.registrationStartDate = registrationStartDate;
	}

	public String getRegistrationEndDate() {
		return registrationEndDate;
	}

	public void setRegistrationEndDate(String registrationEndDate) {
		this.registrationEndDate = registrationEndDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		Long date = Long.valueOf(startDate);
		this.startDate = dateFormat.format(new Date(date));
	}

	public String getEndDate() {
		Long date = Long.valueOf(startDate);
		this.startDate = dateFormat.format(new Date(date));
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCreator() {
		return creator;
	}

	public void setCreator(int creator) {
		this.creator = creator;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public boolean isPaid() {
		return paid;
	}

	public void setPaid(boolean paid) {
		this.paid = paid;
	}

	public boolean isFollowingCreated() {
		return followingCreated;
	}

	public void setFollowingCreated(boolean followingCreated) {
		this.followingCreated = followingCreated;
	}

	public boolean isFollowingAttending() {
		return followingAttending;
	}

	public void setFollowingAttending(boolean followingAttending) {
		this.followingAttending = followingAttending;
	}

	public boolean isAttending() {
		return attending;
	}

	public void setAttending(boolean attending) {
		this.attending = attending;
	}

	public String getLocationDescription() {
		return locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}

	public int[] getAttendees() {
		return attendees;
	}

	public void setAttendees(int[] attendees) {
		this.attendees = attendees;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public boolean isAgeRestriction() {
		return ageRestriction;
	}

	public void setAgeRestriction(boolean ageRestriction) {
		this.ageRestriction = ageRestriction;
	}

	public String getTimezoneLongName() {
		return timezoneLongName;
	}

	public void setTimezoneLongName(String timezoneLongName) {
		this.timezoneLongName = timezoneLongName;
	}

	public String getTimezoneShortName() {
		return timezoneShortName;
	}

	public void setTimezoneShortName(String timezoneShortName) {
		this.timezoneShortName = timezoneShortName;
	}

}

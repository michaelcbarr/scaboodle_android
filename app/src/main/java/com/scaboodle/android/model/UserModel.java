package com.scaboodle.android.model;

import org.json.JSONException;
import org.json.JSONObject;

// get banner http://scaboodle.com/avatars/eventbanner/126/banner

public class UserModel {
	private int id;
	private String forename;
	private String surname;
	private String dob;
	private String email;
	private String country;
	private String bio;

	public UserModel(JSONObject json) throws JSONException {
		setId(json.getInt("id"));
		setForename(json.getString("forename"));
		setSurname(json.getString("surname"));
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

}

package com.scaboodle.android.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationObject {

	private int location_id;
	private String countryCode;
	private String created;
	private double latitude;
	private double longitude;
	private boolean modified;
	private String mapData;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

	public LocationObject() {
	}

	public LocationObject(int location_id, String countryCode, String created,
			double latitude, double longitude, boolean modified, String mapData) {
		setLocation_id(location_id);
		setCountryCode(countryCode);
		setCreated(created);
		setLatitude(latitude);
		setLongitude(longitude);
		setModified(modified);
		setMapData(mapData);
	}
	
	//constructor with json object
	public LocationObject(JSONObject json) throws JSONException{
		setLocation_id(json.getInt("id"));
		setCountryCode(json.getString("countryCode"));
		setCreated(json.getString("created"));
		setLatitude(json.getDouble("latitude"));
		setLongitude(json.getDouble("longitude"));
		//setModified(json.getBoolean("modified"));
		//setMapData(json.getString("mapData"));
	}

	public int getLocation_id() {
		return location_id;
	}

	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		
		Long date = Long.valueOf(created);
		this.created = dateFormat.format(new Date(date));
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public boolean isModified() {
		return modified;
	}

	public void setModified(boolean modified) {
		this.modified = modified;
	}

	public String getMapData() {
		return mapData;
	}

	public void setMapData(String mapData) {
		this.mapData = mapData;
	};

}

package com.scaboodle.android.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class EventComment {

	private int itemId;
	private int eventId;
	private int memberId;
	private String comment;
	private String commentDate;
	private String name;
	private String profilePicUrl;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	public EventComment(){
		
	}
	
	public EventComment(JSONObject json, String name) throws JSONException{
		setItemId(json.getInt("id"));
		setEventId(json.getInt("eventId"));
		setMemberId(json.getInt("memberId"));
		setComment(json.getString("comment"));
		setCommentDate(json.getString("commentDate"));
		setName(name);
		setProfilePicUrl(Integer.toString(getMemberId()));
	}


	public int getItemId() {
		return itemId;
	}


	public void setItemId(int itemId) {
		this.itemId = itemId;
	}


	public int getEventId() {
		return eventId;
	}


	public void setEventId(int eventId) {
		this.eventId = eventId;
	}


	public int getMemberId() {
		return memberId;
	}


	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public String getCommentDate() {
		
		return commentDate;
	}


	public void setCommentDate(String commentDate) {
		Long date = Long.valueOf(commentDate);
		this.commentDate = dateFormat.format(new Date(date));
	}

	public String getName() {
		
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String id) {
		this.profilePicUrl = "http://scaboodle.com/avatars/member/"+id+"/large";
	}

}

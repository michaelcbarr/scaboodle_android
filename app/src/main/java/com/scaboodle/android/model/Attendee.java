package com.scaboodle.android.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Attendee {

    private int id;
    private int eventId;
    private int memberId;
    private boolean checkedIn;
    private String memberProfileUrl;
	
	public Attendee(JSONObject json) throws JSONException{
		setId(json.getInt("id"));
		setEventId(json.getInt("eventId"));
		setMemberId(json.getInt("memberId"));
		setCheckedIn(json.getBoolean("checkedIn"));
		setMemberProfileUrl(Integer.toString(getMemberId()));
	}

	public Attendee() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public boolean isCheckedIn() {
		return checkedIn;
	}

	public void setCheckedIn(boolean checkedIn) {
		this.checkedIn = checkedIn;
	}

	public String getMemberProfileUrl() {
		return memberProfileUrl;
	}

	public void setMemberProfileUrl(String memberId) {
		this.memberProfileUrl = "http://scaboodle.com/avatars/member/"+memberId+"/large";
	}
	
}
